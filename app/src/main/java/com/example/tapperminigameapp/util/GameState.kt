package com.example.tapperminigameapp.util

data class GameState(val lives : Int, val comboLength : Int, val monsterTimer : Int, val gameState: Int)
