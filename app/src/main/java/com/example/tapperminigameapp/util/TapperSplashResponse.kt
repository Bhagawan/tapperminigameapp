package com.example.tapperminigameapp.util

import androidx.annotation.Keep

@Keep
data class TapperSplashResponse(val url : String)