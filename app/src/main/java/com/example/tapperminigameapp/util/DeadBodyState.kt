package com.example.tapperminigameapp.util

data class DeadBodyState(val hole : Int, var timer : Int, val size : Float)
