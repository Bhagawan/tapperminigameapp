package com.example.tapperminigameapp

import androidx.lifecycle.ViewModel
import com.example.tapperminigameapp.util.GameState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch

class MainViewModel: ViewModel() {
    private val scope = CoroutineScope(Dispatchers.Default)
    private var mutableGameFlow = MutableSharedFlow<GameState>(1,1)
    val gameFlow : SharedFlow<GameState> = mutableGameFlow.asSharedFlow()

    fun saveState(state: GameState) {
        scope.launch {
            mutableGameFlow.emit(state)
        }
    }
}