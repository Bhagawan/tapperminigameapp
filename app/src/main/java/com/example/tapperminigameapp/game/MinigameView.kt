package com.example.tapperminigameapp.game

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import com.example.tapperminigameapp.R
import com.example.tapperminigameapp.util.Coordinate
import com.example.tapperminigameapp.util.DeadBodyState
import com.example.tapperminigameapp.util.GameState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.math.absoluteValue
import kotlin.math.min

class MinigameView (context: Context, attributeSet: AttributeSet?) : View(context, attributeSet) {
    private var mWidth = 0
    private var mHeight = 0
    private var holeSize = 0.0f

    private val holes = ArrayList<Coordinate>()

    private var lives = 3

    private var comboLength = 0
    private var comboTimer = 0

    private var monsterTimer = -1
    private var currentHole = 0
    private var monsterMaxLiveTime = 120
    private val deadBodies = ArrayList<DeadBodyState>()

    private val restartBitmap = AppCompatResources.getDrawable(context, R.drawable.ic_baseline_replay)?.toBitmap() ?: Bitmap.createBitmap(1,1,Bitmap.Config.ARGB_8888)
    private val holeBitmap = BitmapFactory.decodeResource(context.resources, R.drawable.hole)
    private val monsterBitmap = BitmapFactory.decodeResource(context.resources, R.drawable.monster)
    private val monsterDeadBitmap = BitmapFactory.decodeResource(context.resources, R.drawable.monster_dead)
    private val heartBitmap = BitmapFactory.decodeResource(context.resources, R.drawable.heart)

    private var state = GAME

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom

            val pad = 45
            holeSize = (min(mWidth, mHeight) - pad * 3) / 2.0f
            for( x in pad..((mWidth - holeSize).toInt()) step (holeSize.toInt() + pad)) {
                for( y in pad..(mHeight - holeSize.toInt()) step (holeSize.toInt() + pad)) holes.add(Coordinate(x + holeSize.toInt() / 2, y + holeSize.toInt() / 2))
            }
        }
    }

    companion object {
        const val GAME = 0
        const val END_SCREEN = 1
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            if(lives <= 0) {
                saveRecord(comboLength)
                state = END_SCREEN
            }
            drawTable(it)
            drawUI(it)
            if(state == GAME) {
                updateMonsters()
                drawMonsters(it)
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                when(state) {
                    GAME -> {

                    }
                    END_SCREEN -> {

                    }
                }
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                when(state) {
                    GAME -> {

                    }
                    END_SCREEN -> {

                    }
                }
                return true
            }
            MotionEvent.ACTION_UP -> {
                when(state) {
                    GAME -> {
                        press(event.x, event.y)
                    }
                    END_SCREEN -> {
                        restart()
                    }
                }
                return true
            }
        }
        return super.onTouchEvent(event)
    }

    // Public functions

    fun saveState() : GameState = GameState(lives, comboLength, monsterTimer, state)

    fun restoreState(newState: GameState) {
        lives = newState.lives
        comboLength = newState.comboLength
        state = newState.gameState
        monsterTimer = newState.monsterTimer
    }

    // Private functions

    private fun drawTable(c : Canvas) {
        val p = Paint()
        val s = (holeSize / 2).toInt()
        for(hole in holes) c.drawBitmap(holeBitmap, null, Rect(hole.x - s, hole.y - s, hole.x + s, hole.y + s), p)
    }

    private fun drawUI(c : Canvas) {
        val p = Paint()
        p.isFakeBoldText = true
        p.color = Color.WHITE
        p.textSize = 90.0f
        p.textAlign = Paint.Align.CENTER
        p.textScaleX = 1.3f
        when(state) {
            GAME -> {
                if(comboTimer > 0) {
                    c.drawText("$comboLength", mWidth / 2.0f, 100.0f, p)
                    p.color = Color.YELLOW
                    p.textSize = 80.0f
                    c.drawText("$comboLength", mWidth / 2.0f, 100.0f, p)
                    comboTimer--
                }
                val liveSize = holeSize * 0.2f
                if(mWidth < mHeight) {
                    for(n in 0 until lives) {
                        val left = mWidth / 2.0f - (lives * liveSize / 2.0f) + n * liveSize
                        c.drawBitmap(heartBitmap, null, Rect(left.toInt(), (mHeight - liveSize).toInt(),
                            (left + liveSize).toInt(), mHeight), p)
                    }
                } else {
                    for(n in 0 until lives) {
                        val top = mHeight / 2.0f - (lives * liveSize / 2.0f) + n * liveSize
                        c.drawBitmap(heartBitmap, null, Rect(mWidth - liveSize.toInt(), top.toInt(),
                            mWidth, (top + liveSize).toInt()
                        ), p)
                    }
                }
            }
            END_SCREEN -> {
                p.style = Paint.Style.FILL
                p.color = ResourcesCompat.getColor(context.resources, R.color.dark_grey, null)
                val size = min(mWidth, mHeight) * 0.5f
                c.drawRoundRect((mWidth - size) / 2.0f,
                    mHeight  * 0.2f,
                    (mWidth + size) * 0.5f, mHeight  * 0.8f, 20f, 20f, p)
                p.color = Color.YELLOW
                p.style = Paint.Style.STROKE
                c.drawRoundRect((mWidth - size) / 2.0f,
                    mHeight  * 0.2f,
                    (mWidth + size) * 0.5f, mHeight  * 0.8f, 20f, 20f, p)

                p.textSize = 40.0f
                p.style = Paint.Style.FILL
                p.color = Color.WHITE
                c.drawText("Ваш счет: $comboLength", mWidth / 2.0f, mHeight * 0.3f, p)
                p.color = Color.YELLOW
                c.drawText("Рекорд: ${getRecord()}", mWidth / 2.0f, mHeight * 0.3f + 50, p)

                val restartSize = size - 60

                c.drawBitmap(restartBitmap, null, Rect(((mWidth - restartSize) / 2).toInt(),
                    (mHeight * 0.8f - restartSize - 30).toInt(),
                    ((mWidth + restartSize) / 2).toInt(), (mHeight * 0.8f - 30).toInt()), p)
            }
        }
    }

    private fun drawMonsters(c : Canvas) {
        val p = Paint()
        if(monsterTimer > 0) {
            val size = when(monsterTimer) {
                in 0..(monsterMaxLiveTime * 0.1f).toInt() -> holeSize * 0.3f * (monsterTimer / (holeSize * 0.1f))
                in (monsterMaxLiveTime * 0.9f).toInt()..monsterMaxLiveTime ->  holeSize * 0.3f * ((monsterMaxLiveTime - monsterTimer).absoluteValue / (holeSize * 0.1f))
                else -> holeSize * 0.3f
            }
            if(currentHole in holes.indices) c.drawBitmap(monsterBitmap, null, Rect((holes[currentHole].x - size).toInt(),
                (holes[currentHole].y - size).toInt(), (holes[currentHole].x + size).toInt(), (holes[currentHole].y + size).toInt()), p)
        }
        var n = 0
        while(n < deadBodies.size) {
            if(deadBodies[n].timer > 0) {
                c.drawBitmap(monsterDeadBitmap, null, Rect((holes[deadBodies[n].hole].x - deadBodies[n].size).toInt(),
                    (holes[deadBodies[n].hole].y - deadBodies[n].size).toInt(),
                    (holes[deadBodies[n].hole].x + deadBodies[n].size).toInt(),
                    (holes[deadBodies[n].hole].y + deadBodies[n].size).toInt()), p)
                deadBodies[n].timer--
            } else {
                deadBodies.removeAt(n)
                n--
            }
            n++
        }

    }

    private fun updateMonsters() {
        if(monsterTimer == 0) {
            lives--
        }
        if(monsterTimer-- < -10) {
            if(monsterMaxLiveTime > 20) monsterMaxLiveTime--
            monsterTimer = monsterMaxLiveTime
            currentHole = holes.indices.random()
            var clear = -1
            for(body in deadBodies) if(body.hole == currentHole) clear = deadBodies.indexOf(body)
            if(clear >= 0) deadBodies.removeAt(clear)
        }
    }

    private fun press(x : Float, y : Float) {
        if(x in (holes[currentHole].x - holeSize)..(holes[currentHole].x + holeSize) && y in (holes[currentHole].y - holeSize)..(holes[currentHole].y + holeSize)) {
            val size = when(monsterTimer) {
                in 0..(monsterMaxLiveTime * 0.1f).toInt() -> holeSize * 0.3f * (monsterTimer / (holeSize * 0.1f))
                in (monsterMaxLiveTime * 0.9f).toInt()..monsterMaxLiveTime ->  holeSize * 0.3f * ((monsterMaxLiveTime - monsterTimer).absoluteValue / (holeSize * 0.1f))
                else -> holeSize * 0.3f
            }
            deadBodies.add(DeadBodyState(currentHole, 30, size))
            monsterTimer = -30
            comboLength++
            comboTimer = 30
        } else {
            //lives--
        }
    }

    private fun restart() {
        comboLength = 0
        comboTimer = 0
        monsterTimer = -30
        deadBodies.clear()
        lives = 3
        state = GAME
    }

    private fun getRecord() : Int = context.getSharedPreferences("AppData", Context.MODE_PRIVATE).getInt("record", 0)

    private fun saveRecord(record : Int) {
        val shP = context.getSharedPreferences("AppData", Context.MODE_PRIVATE)
        if(shP.getInt("record", 0) < record) shP.edit().putInt("record", record).apply()
    }


}